<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => '/',
    'uses' => 'MainController@index'
])->middleware("log");

Route::get("/login", [
    'as' => 'login.index',
    'uses' => 'MainController@login'
]);

Route::POST("/login/comprobar", [
    'as' => 'login.check',
    'uses' => 'MainController@check'
]);

Route::get("/login/salir", [
    'as' => 'login.salir',
    'uses' => 'MainController@salir'
])->middleware("log");

//Rutas de localizaciones
Route::get('localizaciones', [
    'as' => 'localizaciones',
    'uses' => 'LocalizacionController@index'
])->middleware("log");
Route::get('localizaciones/borrar/{id}', [
    'as' => 'localizaciones.destroy',
    'uses' => 'LocalizacionController@destroy'
])->middleware("log");
Route::get('localizaciones/editar/{id}', [
    'as' => 'localizaciones.edit',
    'uses' => 'LocalizacionController@edit'
])->middleware("log");
Route::get('localizaciones/crear', [
    'as' => 'localizaciones.create',
    'uses' => 'LocalizacionController@create'
])->middleware("log");
Route::POST('localizaciones/guardar', [
    'as' => 'localizaciones.store',
    'uses' => 'LocalizacionController@store'
])->middleware("log");
Route::POST('localizaciones/actualizar/{id}', [
    'as' => 'localizaciones.update',
    'uses' => 'LocalizacionController@update'
])->middleware("log");
Route::get('localizaciones/buscar', [
    'as' => 'localizaciones.show',
    'uses' => 'LocalizacionController@show'
])->middleware("log");

//Rutas de usuarios
Route::get('usuarios', [
    'as' => 'usuarios',
    'uses' => 'UsuarioController@index'
])->middleware("log");
Route::get('usuarios/borrar/{id}', [
    'as' => 'usuarios.destroy',
    'uses' => 'UsuarioController@destroy'
])->middleware("log");
Route::get('usuarios/editar/{id}', [
    'as' => 'usuarios.edit',
    'uses' => 'UsuarioController@edit'
])->middleware("log");
Route::get('usuarios/crear', [
    'as' => 'usuarios.create',
    'uses' => 'UsuarioController@create'
])->middleware("log");
Route::POST('usuarios/guardar', [
    'as' => 'usuarios.store',
    'uses' => 'UsuarioController@store'
])->middleware("log");
Route::POST('usuarios/actualizar/{id}', [
    'as' => 'usuarios.update',
    'uses' => 'UsuarioController@update'
])->middleware("log");
Route::get('usuarios/buscar', [
    'as' => 'usuarios.show',
    'uses' => 'UsuarioController@show'
])->middleware("log");