<?php

use Illuminate\Database\Seeder;

class LocalizacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ayuda a rellenar la tabla
         for ($i = 1; $i < 50; $i++) {
         	\DB::table('localizaciones')->insert(array('id'=>$i,'nom'=>'localizacion'.$i,));
         }
    }
}
