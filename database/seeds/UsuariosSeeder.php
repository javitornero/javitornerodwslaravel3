<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ayuda a rellenar la tabla
         for ($i = 1; $i < 50; $i++) {
         	\DB::table('usuarios')->insert(array('id'=>$i,'nom'=>'usuario'.$i,'localizacion_id'=>$i ));
         }
    }
}
