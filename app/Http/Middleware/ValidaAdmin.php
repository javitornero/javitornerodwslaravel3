<?php

namespace App\Http\Middleware;

use Closure;

class ValidaAdmin
{
    public function handle($request, Closure $next)
    {
        if(!session()->get('auth') || session()->get('auth')!=="autenticado") {
            return redirect()->route('login.index');
        }
        return $next($request);
    }
}
