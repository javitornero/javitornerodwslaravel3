<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    protected $admin = "admin";
    protected $password = "admin";

    public function index()
    {
        return view('inicio');
    }
    
    public function login()
    {
        return view('login');
        
    }
    
    
    public function check(Request $request)
    {

        if ($request->nom == $this->admin && $request->password == $this->password) {
            $request->session()->put('auth', 'autenticado');
            return redirect()->route('/');
        } else {
            return redirect()->route('login.index');
        }
        
    }
    
    public function salir()
    {
        session()->forget('auth');
        return redirect()->route('login.index');
        
    }
}
