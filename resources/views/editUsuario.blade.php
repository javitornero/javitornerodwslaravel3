@extends('layout')

@section('titulo', 'Editar usuario')

@section('contenido')

<div class="container width45">
    <h1>Editar Usuario</h1>
    <form action="{{ route('usuarios.update', $usuario->id) }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        <input name="id" type="hidden" value="{{ $usuario->id }}"  readonly="readonly" />
        <div class="form-group">
            <label for="nom">Nombre nuevo usuario:</label>
            <input type="text" class="form-control" id="nom" placeholder="Nombre" name="nom" value="{{ $usuario->nom }}" required>
        </div>
        <div class="form-group">
            <label>Localización del usuario</label>
            <select class="form-control" name="localizacion" required > <!-- Importante que el name del select sea localizacion y no localizacion_id -->
                <option value="0">-- Escoge una localización --</option>
                @foreach ($localizaciones as $localizacion)
                    @if ($usuario->localizacion_id == $localizacion->id)        
                        <option value="{{ $localizacion->id }}" selected>
                    @else
                        <option value="{{ $localizacion->id }}">
                    @endif
                        {{ $localizacion->nom }}
                        </option>
                @endforeach;  
            </select>
        </div>
        <button type="submit" class="btn btn-default">Guardar</button>
    </form>
</div>
@endsection
