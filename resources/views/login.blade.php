@extends('layout')

@section('titulo', 'Login')

@section('contenido')
	<div class="container width45">
		<h1>Control de acceso</h1>
		<form method="POST" action="{{route('login.check')}}" >
	        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
			<div class="form-group">
				<label for="nom">Nombre:</label>
	            <input type="text" class="form-control width16rem" placeholder="Nombre" name="nom" required autofocus>
	        </div>
	        <div class="form-group">
				<label for="pass">Password:</label>
	            <input type="password" class="form-control width16rem" placeholder="Password" name="password" required >
			</div>
	        <button type="submit" class="btn btn-default">Entrar</button>
		</form>
	</div>
@endsection