@extends('layout')

@section('titulo', 'Ver localizaciones')

@section('contenido')

    <div class="container">
        <div class="table-responsive">
            <h1>Ver localizaciones</h1>
            <table class="table table-striped table-hover table-bordered table-responsive">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Edit</th>
                        <th class="text-center">Delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Edit</th>
                        <th class="text-center">Delete</th>
                    </tr>
                </tfoot>
                <tbody> 
                    @foreach ($localizaciones as $localizacion)
                    <tr>
                        <td class="text-center">{{ $localizacion->id }}</td>
                        <td class="text-center">{{ $localizacion->nom }}</td>
                        <td class="text-center"><a href={{route('localizaciones.edit', $localizacion->id)}}><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                        <td class="text-center"><a href={{route('localizaciones.destroy', $localizacion->id)}}><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection