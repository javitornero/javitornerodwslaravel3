@extends('layout')

@section('titulo', 'Editar localización')

@section('contenido')
<div class="container width45">
    <h1>Editar localización</h1>
    <form action="{{ route('localizaciones.update', $localizacion->id) }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        <input name="id" type="hidden" value="{{ $localizacion->id }}"  readonly="readonly" />
        <div class="form-group">
            <label for="nom">Nombre nueva localización:</label>
            <input type="text" class="form-control" id="nom" placeholder="Nombre" name="nom" value="{{ $localizacion->nom }}" required>
        </div>
        <button type="submit" class="btn btn-default">Guardar</button>
    </form>
</div>
@endsection
