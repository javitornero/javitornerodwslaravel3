@extends('layout')

@section('titulo', 'Crear localización')

@section('contenido')
<div class="container width45">
    <h1>Nueva localización</h1>
    <form action="{{ route('localizaciones.store') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        <input name="id" type="hidden" value="{{ $id }}"  readonly="readonly" />
        <div class="form-group">
            <label for="nom">Nombre nueva localización:</label>
            <input type="text" class="form-control" id="nom" placeholder="Nombre" name="nom" required>
        </div>
        <button type="submit" class="btn btn-default">Guardar</button>
    </form>
</div>
@endsection