@extends('layout')

@section('titulo', 'Crear usuario')

@section('contenido')
<div class="container width45">
    <h1>Nuevo usuario</h1>
    <form action="{{ route('usuarios.store') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        <input name="id" type="hidden" value="{{ $id }}"  readonly="readonly" />
        <div class="form-group">
            <label for="nom">Nombre nuevo usuario:</label>
            <input type="text" class="form-control" id="nom" placeholder="Nombre" name="nom" required>
        </div>        
        <div class="form-group">
            <label>Localización del usuario</label>
            <select class="form-control" name="localizacion_id" required >
                <option  selected disabled>Elige opción</option>
                @foreach ($localizaciones as $localizacion)
                    <option value="{{ $localizacion->id }}">
                        {{ $localizacion->nom }}
                    </option>
                @endforeach; 
            </select>
        </div>
        <button type="submit" class="btn btn-default">Guardar</button>
    </form>
</div>  
@endsection