@extends('layout')

@section('titulo', 'Ver usuarios')

@section('contenido')

    <div class="container">
        <div class="table-responsive">
            <h1>Ver usuarios</h1>
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Locator</th>
                        <th class="text-center">Edit</th>
                        <th class="text-center">Delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Locator</th>
                        <th class="text-center">Edit</th>
                        <th class="text-center">Delete</th>
                    </tr>
                </tfoot>
                <tbody> 
                    @foreach ($usuarios as $usuario)
                    <tr>
                        <td class="text-center">{{ $usuario->id }}</td>
                        <td class="text-center">{{ $usuario->nom }}</td>
                        <td class="text-center">{{ $usuario->localizaciones->nom }}</td>
                        <td class="text-center"><a href={{route('usuarios.edit', $usuario->id)}}><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                        <td class="text-center"><a href={{route('usuarios.destroy', $usuario->id)}}><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection